---
title: 错误集合
date: 2023-02-13 11:02:35
permalink: /language/go/a99ab5/
categories:
  - 语言
  - GoLang
tags:
  - GoLang错误
  - Go错误
---

## 前言

使用`GoLang`中遇到的奇葩问题，本文持续更新

<!-- more -->

<InArticleAdsense
    data-ad-client="ca-pub-1725717718088510"
    data-ad-slot="7426219401">
</InArticleAdsense>

## `conversion from int to string yields a string of one rune, not a string of digits (did you mean fmt.Sprint(x)?)`

翻译：

从`int`到字符串的转换产生一个符文字符串，而不是一个数字字符串(你是指`fmt.Sprint(x)`吗?)

解决方案：

``` go
// 产生错误的写法，会导致编译不通过
func Day() string {
	return string(time.Now().Day())
}

// 正确的写法
func Day() string {
	return fmt.Sprintf("%v", time.Now().Day())
}
```

## `missing dot in first path element`问题解决

我出现这个原因是使用开源代码的时候将原始的引用改变了但是忘记改变`go.mod`中的嵌套引用了。

### 问题原因
因为在`go1.13`中，`go module`名称规范要求路径的第一部分必须满足域名规范，否则可能汇报类似

`malformed module path "xxxx": missing dot in first path element`这样的错误。


