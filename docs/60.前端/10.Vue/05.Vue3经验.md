---
title: Vue3经验
date: 2022-11-21 13:55:21
permalink: /language/vue/ba496f/
categories:
  - 前端
  - Vue
tags:
  - Vue3
  - 经验
  - 路径使用@
  - axios跨域
---

## 前言

杂七杂八的小技巧太特么多了，就一篇文章多多记录吧🐶

<!-- more -->

<InArticleAdsense
    data-ad-client="ca-pub-1725717718088510"
    data-ad-slot="7426219401">
</InArticleAdsense>

## `Vue3+Vite`配置路径使用别名`@`

1. 安装`@types/node`
    ``` shell
    # 安装@types/node
    npm install @types/node --save-dev
    ```

2. 修改`vite.config.js`，也不知道咋描述，对着下面的抄吧，我是新手安心食用
    ``` js
    import { defineConfig } from "vite";
    import vue from "@vitejs/plugin-vue";
    // 👇这是新增的
    import path from "path"; //这个path用到了上面安装的@types/node

    export default defineConfig({
        plugins: [vue()],
        // 👇这是新增的
        resolve: {
            alias: {
            '@': path.resolve('./src') // @代替src
            }
        }
    })
    ```

## `Vue3+Vite`全局使用`axios`，解决`axios`跨域问题

1. 安装`axios`

    ``` shell
        npm i axios --save
    ```

2. 在`main.js`中引入`axios`

    ``` shell
        import { createApp } from "vue";
        import App from "./App.vue";
        // 👆的不用管
        // 引入axios
        import axios from "axios";

        const app = createApp(App);

        app.config.globalProperties.$http = axios;
        app.mount("#app");
    ```

3. 【解决跨域】修改`vite.config.js`

    ``` js
        import { defineConfig } from "vite";
        import vue from "@vitejs/plugin-vue";

        // https://vitejs.dev/config/
        export default defineConfig({
        plugins: [vue()],
        server: {
            proxy: {
                <!-- 此处的/api是取的别名，等同与下面的target路径，在实际中使用的使用/api/ -->
                "/api": {
                    target: "http://127.0.0.1:8088/test/api", //实际请求地址
                    <!-- 这里算是处理跨域的核心 -->
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/api/, ""),
                },
            },
        },
        });
    ```

4. 使用

    ``` js
        <!-- 我服务端的实际路径为 http://127.0.0.1:8088/test/api/general/getUser -->
        import { getCurrentInstance } from "vue";
        function getOrderList() {
            // 临时默认为1000004
            proxy.$http.get("/api/general/getUser").then((response) => {
                console.log(response);
            });
        }
    ```

## 使用`<script setup>`时如何使用`onMounted`

``` javascript
<script setup>
import {onMounted} from "vue"

function GNews() {
  console.log("1111")
}

onMounted(()=>{
  GNews()
})
</script>
```

## `Vue`中`href`的链接拼接

``` javascript
// 1. 使用:href而不是href
// 2. 外部的双引号只是语法格式
<a :href="'https://www.juliangip.com/news/'+obj.id+'.html'"></a>
```

## `elementui`中`el-radio`不执行`@click`

将`@click`变更为`@click.native`

## `elementui`中`el-radio-group`内部的`el-radio`值改变如何传值

``` javascript
@change="((label)=>{changeValue(label, item, index)})"
```